function navigation(n){
    var textHTML = '<nav>';
    textHTML += '<ul class="nav_list">';
    textHTML += '<li>';
    //ABOUT PAGE
    if(n==1){
        textHTML += '<image src="./images/SVG/box_About_on.svg" class="box cls_box_menu"/>';

    }else{
        textHTML += '<a href="./about.html">';
        textHTML += '<image src="./images/SVG/box_About_off.svg" class="box cls_box_menu"/>';
        textHTML += '</a>';
    }
    textHTML += '</li>';
    textHTML += '<li>';
    //NEWS PAGE
    if(n==2){
        textHTML += '<image src="./images/SVG/box_News_on.svg" class="box cls_box_menu"/>';
    }else{
        textHTML += '<a href="./news.html">';
        textHTML += '<image src="./images/SVG/box_News_off.svg" class="box cls_box_menu"/>';
        textHTML += '</a>';
    }
    textHTML += '</li>';
    textHTML += '<li>';
    if(n==3){
        textHTML += '<image src="./images/SVG/box_rd_on.svg" class="box cls_box_menu"/>';
    }else{
        textHTML += '<a href="./rd.html">';
        textHTML += '<image src="./images/SVG/box_rd_off.svg" class="box cls_box_menu"/>';
        textHTML += '</a>';
    }
    textHTML += '</li>';
    textHTML += '<li>';
    if(n==4){
        textHTML += '<image src="./images/SVG/box_PP_on.svg" class="box cls_box_menu" />';
    }else{
        textHTML += '<a href="./privacy_policy.html">';
        textHTML += '<image src="./images/SVG/box_PP_off.svg" class="box cls_box_menu" />';
        textHTML += '</a>';
    }
    textHTML += '</li>';
    textHTML += '</ul>';
    textHTML += '</nav>';
    document.getElementById("navigation").innerHTML = textHTML;
}
