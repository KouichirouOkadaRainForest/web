$(function() {
    //ハンバーガーバー処理
    $('.hamburger').click(function() {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $('.globalMenuSp').addClass('active');
        } else {
            $('.globalMenuSp').removeClass('active');
        }
    });
    //メニュー内を閉じておく
    $('.globalMenuSp a[href]').click(function() {
        $('.globalMenuSp').removeClass('active');
       $('.hamburger').removeClass('active');

    });
    //ナビゲーションホバー処理
    $('.box').each(function() {
        let img_off = $(this).attr('src');
        let img_on = $(this).attr('src').replace('off', 'on');
          $(this).hover(
            function () {
              $(this).attr('src', img_on);
            },
            function () {
              $(this).attr('src', img_off);
            }
          );
      });
});
